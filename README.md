myAVR-MK3 - C++ Example - Interrupt
===================================

  How to implement an interrupt object orientated with avr-g++


Copyright (c) 2017 Martin Singer <martin.singer@web.de>

Licensed unter the terms of the GNU GPL3+


About
-----

This is a implementation based on the first example by Christian M.,
about using interrupt vectors with C++, published on "mikrocontroller.net":
<https://www.mikrocontroller.net/articles/AVR_Interrupt_Routinen_mit_C%2B%2B>


### Function

A binary counter driven by interrupt is shown by LEDs.


### Links

* <https://www.mikrocontroller.net/articles/AVR_Interrupt_Routinen_mit_C%2B%2B>

* <http://www.nongnu.org/avr-libc/user-manual/group__avr__interrupts.html>
* <https://gcc.gnu.org/onlinedocs/gcc-4.7.2/gcc/Function-Attributes.html>
* <https://gcc.gnu.org/onlinedocs/gcc-4.7.2/gcc/Asm-Labels.html>
* <http://www.embedded.com/design/prototyping-and-development/4023817/Interrupts-in-C->
* <http://www.embedded.com/design/embedded/4023817/2/Interrupts-in-C->

* <http://www.atmel.com/devices/ATMEGA2560.aspx>


Configuration
-------------

* mySmartUSB MK3 programmer
* myAVR MK3 board
* Breadboard with TWI port extension ICs
* Key switch matrix board (KMX)


Creating the Doxygen reference
------------------------------

	$ doxygen doxygen.conf
	$ firefox doc/html/index.html

Creates a directory "doc" with the source documentation,
created from the doxygen comments inside the source files
and the README.md file.


Compiling and Programming
-------------------------

Install the `avr-gcc` (compiler) and the `avrdude` (programmer).
Have a look at the Makefile and adjust it to your belongs.
Possibly change the programmer.


	$ cd src
	$ make clean
	$ make
	$ make program


Circuits
--------

### The Board

[board]: http://shop.mymcu.de/Systemboards%20und%20Programmer/myAVR%20Board%20MK3%20256K%20PLUS.htm?sp=article.sp.php&artID=100063 "myAVR Board MK3 256K PLUS"
[programmer]: http://shop.mymcu.de/systemboards%20and%20programmer/mySmartUSB%20MK3%20(Programmer%20und%20Bridge).htm?sp=article.sp.php&artID=100058 "mySmartUSB MK3"
[mcu]: http://www.atmel.com/devices/ATMEGA2560.aspx "ATmega2560"

* Board: [myAVR Board MK3 256K PLUS][board]
* Programmer: [mySmartUSB MK3][programmer]
* MCU: [Atmel ATmega2560, 16MHz][mcu]

