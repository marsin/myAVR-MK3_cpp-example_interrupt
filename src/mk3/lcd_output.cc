/******************************************************************************
 * myAVR-MK3_cpp-driver
 * ====================
 *
 *   - C++ driver for the MyAVR MK3 development board hardware
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3_cpp-driver.
 *
 *    MyAVR-MK3_cpp-driver is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MyAVR-MK3_cpp-driver is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MyAVR-MK3_cpp-driver.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

/// Primitive output functions for the LCD on the MyAVR MK3 development board.
///
/// @file lcd_output.cc
/// @author Martin Singer


#include "lcd_output.h"

using namespace MK3;


LcdOutput::LcdOutput() :
	Lcd()
{

}


void
LcdOutput::printChar(char character)
{
	printAsciiCharacter(character);
}


void
LcdOutput::printDecNumber(uint8_t number)
{
	printAsciiCharacter(number / 100 + '0');
	number %= 100;
	printAsciiCharacter(number / 10 + '0');
	number %= 10;
	printAsciiCharacter(number + '0');
}


void
LcdOutput::printHexNumber(uint8_t number)
{
	printAsciiCharacter('0');
	printAsciiCharacter('x');

	if (number < 0x0A) {
		printAsciiCharacter(number / 0x10 + '0');
	} else {
		printAsciiCharacter(number / 0x10 + 'A' - 0x0A);
	}
	if (number < 0x0A) {
		printAsciiCharacter(number % 0x10 + '0');
	} else {
		printAsciiCharacter(number % 0x10 + 'A' - 0x0A);
	}
}


void
LcdOutput::printBinNumber(uint8_t number)
{
	printAsciiCharacter('0');
	printAsciiCharacter('b');

	for (uint8_t i = 7; i > 0; --i) {
		if (number & (1<<i)) {
			printAsciiCharacter('1');
		} else {
			printAsciiCharacter('0');
		}
	}
	if (number & (1<<0)) {
			printAsciiCharacter('1');
		} else {
			printAsciiCharacter('0');
		}
}


void
LcdOutput::setCursorPosition(uint8_t pos_x, uint8_t pos_y)
{
	setPointerPosition(pos_x * 5, pos_y * 8);
}

