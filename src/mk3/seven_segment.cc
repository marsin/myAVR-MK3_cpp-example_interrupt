/******************************************************************************
 * myAVR-MK3_cpp-driver
 * ====================
 *
 *   - C++ driver for the MyAVR MK3 development board hardware
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3_cpp-driver.
 *
 *    MyAVR-MK3_cpp-driver is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MyAVR-MK3_cpp-driver is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MyAVR-MK3_cpp-driver.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// Driver for the Seven Segment on the MyAVR MK3 development board.
///
/// @file   seven_segment.cc
/// @author Martin Singer


#include <avr/io.h>
#include "board.h"
#include "seven_segment.h"

using namespace MK3;


const uint8_t SevenSegment::Characters[] = {
		0b00111111, // 0
		0b00000110, // 1
		0b01011011, // 2
		0b01001111, // 3
		0b01100110, // 4
		0b01101101, // 5
		0b01111101, // 6
		0b00000111, // 7
		0b01111111, // 8
		0b01101111, // 9
		0b01110111, // A
		0b01111100, // B
		0b00111001, // C
		0b01011110, // D
		0b01111001, // E
		0b01110001, // F
		0b01000000, // -
		0b00000000, //
		0b01001001  // 3 horizontal lines
	};


SevenSegment::SevenSegment()
{
	MK3_SEG7_DDR  = 0xFF; // config PORT as output
	MK3_SEG7_PORT = 0x00; // init output
}


/** Write to seven segment register.
 *
 * @param byte data byte becomes written directly to the port register
 */
void
SevenSegment::writePort(const uint8_t byte)
{
	MK3_SEG7_PORT = byte;
	return;
}


/** Read from seven segment register.
 *
 * @return pointer to the seven segment port register
 */
volatile uint8_t*
SevenSegment::readPort(void)
{
	return &MK3_SEG7_PORT;
}


/** Clear seven segment.
 *
 * Turns all LEDs off
 */
void
SevenSegment::clearSegments(void)
{
	MK3_SEG7_PORT = 0x00;
	return;
}


/** Show the dot of the seven segment.
 *
 * @param dot
 *
 * Shows the dot in dependence of the boolean input value.
 */
void
SevenSegment::showDot(const uint8_t dot)
{
	if (dot) {
		MK3_SEG7_PORT |= 0x80; // 0b10000000
	} else {
		MK3_SEG7_PORT &= 0x7F; // 0b01111111
	}
	return;
}


/** Print a digit (0 to F).
 *
 * @param digit
 *
 * Prints hex digits (0 to F) on seven segment.
 */
void
SevenSegment::printDigit(const uint8_t digit)
{
	uint8_t pattern = DEFAULT_PATTERN;

	if (0x00 <= digit && 0x0F >= digit) {
		pattern = Characters[digit];
	}
	pattern |= (MK3_SEG7_PORT & 0x80); // mask the dot with 0b10000000
	MK3_SEG7_PORT = pattern;
	return;
}


/** Print a character.
 *
 * @param character
 *
 * Prints characters (0-9, a-f, A-F) on seven segment.
 */
void
SevenSegment::printCharacter(const char character)
{
	uint8_t pattern = DEFAULT_PATTERN, // output pattern
	        index = DEFAULT_INDEX;     // signs array index

	if ('0' <= character && '9' >= character) {
		index = (uint8_t) character - '0';        // shows signs '0' to '9'
	} else if ('a' <= character && 'f' >= character) {
		index = (uint8_t) character - 'a' + 0x0A; // shows signs 'A' to 'F'
	} else if ('A' <= character && 'F' >= character) {
		index = (uint8_t) character - 'A' + 0x0A; // shows sings 'A' to 'F'
	}

	if (DEFAULT_INDEX != index) {
		pattern = Characters[index];
	}

	pattern |= (MK3_SEG7_PORT & 0x80); // mask the dot with 0b10000000
	MK3_SEG7_PORT = pattern;

	return;
}
