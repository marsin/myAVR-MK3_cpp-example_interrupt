/******************************************************************************
 * myAVR-MK3_cpp-driver
 * ====================
 *
 *   - C++ driver for the MyAVR MK3 development board hardware
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3_cpp-driver.
 *
 *    MyAVR-MK3_cpp-driver is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MyAVR-MK3_cpp-driver is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MyAVR-MK3_cpp-driver.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// Primitive output functions for the LCD on the MyAVR MK3 development board.
///
/// @file lcd_output.h
/// @author Martin Singer


#ifndef MK3_LCD_OUTPUT_H
#define MK3_LCD_OUTPUT_H

#include "lcd.h"


namespace MK3 {

class LcdOutput : public Lcd {
	public:
		explicit LcdOutput();
//		virtual ~LcdOutput();

		void printChar(char);
		void printDecNumber(uint8_t);
		void printHexNumber(uint8_t);
		void printBinNumber(uint8_t);
		void setCursorPosition(uint8_t, uint8_t);
};

} // namespcae MK3


#endif // MK3_LCD_OUTPUT_H

