/******************************************************************************
 * myAVR-MK3_cpp-interrupt-example
 * =====================
 *
 *   -  myAVR-MK3 - C++ interrupt example implementation
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3_KMX-TWI-INT
 *
 *    myAVR-MK3_cpp-interrupt-example is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    myAVR-MK3_cpp-interrupt-example is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with myAVR-MK3_cpp-interrupt-example.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// myAVR-MK3 - C++ interrupt example implementation
///
/// @file interrupt.cc
/// @author Martin Singer


#include "interrupt.h"


Timer *Timer::TimerInterrupt::ownerTimer = nullptr;


void
Timer::TimerInterrupt::record(Timer *timer)
{
	ownerTimer = timer;
}


void
Timer::TimerInterrupt::serviceRoutine()
{
	if (ownerTimer != nullptr) {
		if (ownerTimer->counter == 0){
			ownerTimer->counter = 255;
		} else {
			--ownerTimer->counter;
		}
	}
}


Timer::Timer()
:
	counter(255)
{
	TimerInterrupt::record(this);

	// Init TIMER3_COMPA_vect
	TCCR3A = (1<<WGM32);                        // CTC mode
//	TCCR3B = (0<<CS32) | (0<<CS31) | (1<<CS30); // prescaler    1 (no prescaler)
//	TCCR3B = (0<<CS32) | (1<<CS31) | (0<<CS30); // prescaler    8
//	TCCR3B = (0<<CS32) | (1<<CS31) | (1<<CS30); // prescaler   64
	TCCR3B = (1<<CS32) | (0<<CS31) | (0<<CS30); // prescaler  256
//	TCCR3B = (1<<CS32) | (0<<CS31) | (1<<CS30); // prescaler 1024
	OCR3A  = 15625;                             // output compare register
	TIMSK3 = (1<<OCIE3A);                       // enable interrupt
}


